Исходники проекта можно распаковать из архива или сделать git clone https://swdmitriy@bitbucket.org/swdmitriy/dvd-manager.git

Для сборки и работы приложения необходимы: maven3, hsqldb

Сборка приложения осуществляется c помощью maven: mvn install
Проект устанавливается в каталог /target. Приложение представляет собой java программу со встроенным вэб-сервером Jetty

Для запуска необходимы следующие файлы:
dvdmanager.jar
application.properties
hibernate.properties

Перед первым запуском необходимо указать следующие свойства:
1. В файле application.properties
- параметры подключения к СУБД (db.driver, db.url, db.username, db.password)
- параметры вэб-сервера (base.url, base.port. По-умолчанию :8080/dvdmanager)
2. В файле hibernate.properties
- диалект текущей БД (org.hibernate.dialect.HSQLDialect)
- режим инициализации начальными данными (hibernate.hbm2ddl.auto). Для заполнения БД тестовыми данными - hibernate.hbm2ddl.auto=create, при повторном запуске - hibernate.hbm2ddl.auto=update) 

Запуск приложения: java -jar dvdmanager.jar

В системе заведены 4 пользователя (логин/пароль):
ivan/1234
peter/1234
john/1234
dmitry/1234

Все пользователи имеют роль USER (админы и гости в системе вообще не предусмотрены).

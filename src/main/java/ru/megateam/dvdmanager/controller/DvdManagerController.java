package ru.megateam.dvdmanager.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import ru.megateam.dvdmanager.entity.Disc;
import ru.megateam.dvdmanager.entity.TakenItem;
import ru.megateam.dvdmanager.exception.DiscTakingException;
import ru.megateam.dvdmanager.service.DiscService;

@Controller
public class DvdManagerController {
 	
	@Autowired
	private DiscService discService;
	
	@RequestMapping(value = "/login", method = RequestMethod.GET)
    public String loginPage(ModelMap model){
        return "login";
    }

	@RequestMapping("/")
    public String showFreeDiscs(ModelMap model) {
		model.put("disc", new Disc());
        model.put("discList", discService.getFreeDisсs());
        return "index"; 
    }
	
	@RequestMapping("/hold")
    public String showTakenByAnotherUsersDisсs(ModelMap model) {
        model.put("discList", discService.getTakenFromUserDisсs());
        return "hold"; 
    }	
	
	@RequestMapping(value = "/newdisc", method = RequestMethod.GET)
    public String showNewDisс(ModelMap model) {
		model.put("disc", new Disc());
        return "newdisc"; 
    }
	
	@RequestMapping(value = "/newdisc", method = RequestMethod.POST)
	public String addDisc(@Valid @ModelAttribute("disc") Disc disс,
			BindingResult result, 
			final RedirectAttributes redirectAttributes) {
		if (result.hasErrors()) {
            return "newdisc";
        } else {
			redirectAttributes.addFlashAttribute("css", "success");
			redirectAttributes.addFlashAttribute("msg", "Disc added successfully!");
			discService.addDisc(disс);
        }
		return "redirect:/";
	}
	
	@RequestMapping(value = "/takedisc", method = RequestMethod.POST)
	public String takeDisc(@ModelAttribute("disc") Disc disc, 
			BindingResult result) throws DiscTakingException {
		discService.takeDisc(disc);
		return "redirect:/";
	}
	@RequestMapping(value = "/takenbyme", method = RequestMethod.GET)
    public String showTakenByMeDisсs(ModelMap model) { 	
		model.put("takenItem", new TakenItem());
        model.put("discList", discService.getTakenByUserDiscs());
        return "takenbyme"; 
    }
	
	@RequestMapping(value = "/freedisc", method = RequestMethod.POST)
	public String freeDisc(@ModelAttribute("takenItem") TakenItem takenItem, 
			BindingResult result) {
		discService.freeDisc(takenItem);
		return "redirect:/takenbyme";
	}
	
	@ExceptionHandler(DiscTakingException.class)
	public String dataConflict() {
		return "error409";
	}
	
}
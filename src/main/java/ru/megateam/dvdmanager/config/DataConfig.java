package ru.megateam.dvdmanager.config;

import org.apache.commons.dbcp2.BasicDataSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
 
import javax.sql.DataSource;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
 
@Configuration
@EnableTransactionManagement
@ComponentScan("ru.megateam.dvdmanager")
@EnableJpaRepositories("ru.megateam.dvdmanager.repository")
public class DataConfig {
    
	private Properties properties;
	
    @Bean
    public DataSource dataSource() {
		properties = new Properties();
    	InputStream is;
		try {
			is = new FileInputStream("application.properties");
		} catch (FileNotFoundException e) {
			throw new IllegalArgumentException("Can't find 'application.properties'", e);
		}
		try {
			properties.load(is);
			is.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
        BasicDataSource ds = new BasicDataSource();
        ds.setUrl(properties.getProperty("db.url"));
        ds.setDriverClassName(properties.getProperty("db.driver"));
        ds.setUsername(properties.getProperty("db.username"));
        ds.setPassword(properties.getProperty("db.password"));

        ds.setInitialSize(Integer.valueOf(properties.getProperty("db.initialSize")));
        ds.setMinIdle(Integer.valueOf(properties.getProperty("db.minIdle")));
        ds.setMaxIdle(Integer.valueOf(properties.getProperty("db.maxIdle")));
        ds.setTimeBetweenEvictionRunsMillis(Long.valueOf(properties.getProperty("db.timeBetweenEvictionRunsMillis")));
        ds.setMinEvictableIdleTimeMillis(Long.valueOf(properties.getProperty("db.minEvictableIdleTimeMillis")));
        ds.setTestOnBorrow(Boolean.valueOf(properties.getProperty("db.testOnBorrow")));
        ds.setValidationQuery(properties.getProperty("db.validationQuery"));

        return ds;
    }

    
    @Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactory() {
        LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
        em.setDataSource(dataSource());
        em.setPackagesToScan(properties.getProperty("db.entity.package"));
        em.setJpaVendorAdapter(new HibernateJpaVendorAdapter());
        em.setJpaProperties(getHibernateProperties());

        return em;
    }

    
    @Bean
    public PlatformTransactionManager transactionManager() {
        JpaTransactionManager manager = new JpaTransactionManager();
        manager.setEntityManagerFactory(entityManagerFactory().getObject());

        return manager;
    }

    public Properties getHibernateProperties() {
        try {
            Properties properties = new Properties();
            InputStream is = new FileInputStream("hibernate.properties");
            properties.load(is);

            return properties;
        } catch (IOException e) {
            throw new IllegalArgumentException("Can't find 'hibernate.properties' in classpath!", e);
        }
    }
    
    

}

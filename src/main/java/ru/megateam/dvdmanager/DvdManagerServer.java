package ru.megateam.dvdmanager;

import java.io.FileInputStream;
import java.io.InputStream;
import java.net.URI;
import java.util.Properties;

import org.apache.log4j.PropertyConfigurator;
import org.eclipse.jetty.annotations.AnnotationConfiguration;
import org.eclipse.jetty.annotations.ClassInheritanceHandler;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.util.ConcurrentHashSet;
import org.eclipse.jetty.util.resource.Resource;
import org.eclipse.jetty.webapp.Configuration;
import org.eclipse.jetty.webapp.WebAppContext;
import org.eclipse.jetty.webapp.WebXmlConfiguration;
import org.springframework.web.WebApplicationInitializer;

import ru.megateam.dvdmanager.config.AppInitializer;
import ru.megateam.dvdmanager.config.SecurityInitializer;


public class DvdManagerServer
{
	public static void main( String[] args ) throws Exception
    {
		Properties properties = new Properties();
		InputStream is = new FileInputStream("application.properties");
		properties.load(is);
		is.close();
		PropertyConfigurator.configure(properties);
		WebAppContext webAppContext = new WebAppContext();
		String webDir = DvdManagerServer.class.getClassLoader().getResource("ru/megateam/dvdmanager").toExternalForm();
		webAppContext.setResourceBase(webDir);
		webAppContext.setContextPath(properties.getProperty("base.url"));		
		final Resource base = Resource.newClassPathResource(".");
		if (base != null) {
			webAppContext.setBaseResource(base);
		} else {    
		    final URI uri = DvdManagerServer.class.getProtectionDomain().getCodeSource().getLocation().toURI();
		    webAppContext.setBaseResource(Resource.newResource("jar:" + uri + "!/"));
		}		
		webAppContext.setConfigurations(new Configuration[] {
			new WebXmlConfiguration(),
			new AnnotationConfiguration() {
				@Override
				public void preConfigure(WebAppContext context) {
					ClassInheritanceMap map = new ClassInheritanceMap();
					map.put(WebApplicationInitializer.class.getName(), new ConcurrentHashSet<String>() {{
						add(AppInitializer.class.getName());
						add(SecurityInitializer .class.getName());
					}});
					context.setAttribute(CLASS_INHERITANCE_MAP, map);
					_classInheritanceHandler = new ClassInheritanceHandler(map);
				}
			}
		});		
		webAppContext.setParentLoaderPriority(true);
		Server server = new Server(Integer.parseInt(properties.getProperty("base.port")));
		server.setHandler(webAppContext);
		server.start();     
		server.join();
	}
    
    
}

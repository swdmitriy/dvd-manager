package ru.megateam.dvdmanager.service;

import org.springframework.data.repository.query.Param;

import ru.megateam.dvdmanager.entity.User;

public interface UserService {
	
    User getByLogin(@Param("login") String name);

}

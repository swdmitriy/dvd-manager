package ru.megateam.dvdmanager.service;

import java.util.List;

import ru.megateam.dvdmanager.entity.Disc;
import ru.megateam.dvdmanager.entity.TakenItem;
import ru.megateam.dvdmanager.exception.DiscTakingException;

public interface DiscService {
	
	Disc getById(long id);
	List<Object> getFreeDisсs();
	List<Object> getTakenFromUserDisсs();
	List<Disc> getTakenByUserDiscs();
	Disc addDisc(Disc disc);
	TakenItem takeDisc(Disc disc) throws DiscTakingException;
	void freeDisc(TakenItem takenItem);

}

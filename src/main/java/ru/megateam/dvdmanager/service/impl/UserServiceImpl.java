package ru.megateam.dvdmanager.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ru.megateam.dvdmanager.entity.User;
import ru.megateam.dvdmanager.repository.UserRepository;
import ru.megateam.dvdmanager.service.UserService;

@Service
public class UserServiceImpl implements UserService {
	
	@Autowired
    private UserRepository userRepository;
	
	@Override
	public User getByLogin(String login) {
		return userRepository.findUserByLogin(login);
	}

	
	

}

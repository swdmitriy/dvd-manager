package ru.megateam.dvdmanager.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ru.megateam.dvdmanager.entity.Disc;
import ru.megateam.dvdmanager.entity.TakenItem;
import ru.megateam.dvdmanager.entity.User;
import ru.megateam.dvdmanager.exception.DiscTakingException;
import ru.megateam.dvdmanager.repository.DiscRepository;
import ru.megateam.dvdmanager.repository.TakenItemRepository;
import ru.megateam.dvdmanager.service.DiscService;
import ru.megateam.dvdmanager.service.UserService;

@Service
public class DiscServiceImpl implements DiscService {
	
	@Autowired
	private DiscRepository disсRepository;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private TakenItemRepository takenItemRepository;

	@Transactional
	@Override
	public Disc addDisc(Disc disc) {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		String currentUserLogin = authentication.getName();
		User currentUser = userService.getByLogin(currentUserLogin);
		disc.setOwnerUser(currentUser);
		Disc addedDisс = disсRepository.saveAndFlush(disc);
		return addedDisс;
	}

	@Override
	public List<Object> getFreeDisсs() {		
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		String currentUserLogin = null;
		if (authentication !=null) {
			currentUserLogin = authentication.getName();
		}
		return disсRepository.getFreeDiscs(currentUserLogin);
	}

	@Override
	public List<Object> getTakenFromUserDisсs() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		String currentUserLogin = authentication.getName();
		return disсRepository.getTakenFromUserDiscs(currentUserLogin);
	}

	@Override
	public List<Disc> getTakenByUserDiscs() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		String currentUserLogin = authentication.getName();
		return disсRepository.getTakenByUserDiscs(currentUserLogin);
	}
	
	@Transactional(readOnly = false, rollbackFor=DiscTakingException.class)
	@Override
	public TakenItem takeDisc(Disc disc) throws DiscTakingException {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		String currentUserLogin = authentication.getName();
		User user = userService.getByLogin(currentUserLogin);
		TakenItem takenDisc = new TakenItem(user, disc);
		try{
			takenItemRepository.saveAndFlush(takenDisc);
		} catch (DataIntegrityViolationException e) {		//неудачная попытка взять уже занятый кем-то диск 
			throw new DiscTakingException(e.getMessage());
		} 
		
		return null;
	}

	@Override
	public Disc getById(long id) {
		disсRepository.getOne(id);
		return null;
	}

	@Transactional
	@Override
	public void freeDisc(TakenItem takenItem) {
		takenItemRepository.delete(takenItem);		
	}

}

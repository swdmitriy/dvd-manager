package ru.megateam.dvdmanager.repository;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import ru.megateam.dvdmanager.entity.Disc;

@Repository
public interface DiscRepository extends JpaRepository<Disc, Long>{
	
	@Query("SELECT d, CASE WHEN (u.login = :login) THEN 'my' ELSE 'not my' END from Disc d LEFT JOIN d.takenDiscs t LEFT JOIN d.ownerUser u WHERE t.disc is null ORDER BY d.name") 
	List<Object> getFreeDiscs(@Param("login") String login);
	@Query("SELECT d.name, t.holderUser FROM TakenItem t LEFT JOIN t.disc d LEFT JOIN d.ownerUser u WHERE u.login = :login ORDER BY d.name")
	List<Object> getTakenFromUserDiscs(@Param("login") String login);	
	@Query("SELECT d, t FROM TakenItem t LEFT JOIN t.disc d LEFT JOIN t.holderUser u WHERE u.login = :login ORDER BY d.name")
	List<Disc> getTakenByUserDiscs(@Param("login") String login);
	
	

}

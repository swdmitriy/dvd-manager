package ru.megateam.dvdmanager.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import ru.megateam.dvdmanager.entity.TakenItem;


public interface TakenItemRepository extends JpaRepository<TakenItem, Long>{

}

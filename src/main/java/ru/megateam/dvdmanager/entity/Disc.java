package ru.megateam.dvdmanager.entity;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "disc")
public class Disc {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
	private long id;

	@NotNull
	@Size(min=2, max=100, message = "Название диска - от 2 до 100 символов")
	@Column(name = "name", length = 100, nullable = false)
	private String name;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "user_id", nullable = false)
	private User ownerUser;
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "disc")
	private Set<TakenItem> takenDiscs = new HashSet<TakenItem>(0);
	
	public Disc() {
	}

	public Disc(long id, String name, User ownerUser) {
		super();
		this.id = id;
		this.name = name;
		this.ownerUser = ownerUser;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public User getOwnerUser() {
		return ownerUser;
	}

	public void setOwnerUser(User ownerUser) {
		this.ownerUser = ownerUser;
	}

	public Set<TakenItem> getTakenDiscs() {
		return takenDiscs;
	}

	public void setTakenDisсs(Set<TakenItem> takenDiscs) {
		this.takenDiscs = takenDiscs;
	}
	
	
	

}

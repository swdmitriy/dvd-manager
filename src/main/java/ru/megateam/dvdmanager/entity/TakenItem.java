package ru.megateam.dvdmanager.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "taken_item")
public class TakenItem {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
	private long id;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "user_id", nullable = false)
	private User holderUser;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "disc_id", nullable = false, unique = true)
	private Disc disc;

	public TakenItem() {
		super();
	}

	public TakenItem(User holderUser, Disc disc) {
		this.holderUser = holderUser;
		this.disc = disc;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public User getHolderUser() {
		return holderUser;
	}

	public void setHolderUser(User holderUser) {
		this.holderUser = holderUser;
	}

	public Disc getDisс() {
		return disc;
	}

	public void setDisc(Disc disc) {
		this.disc = disc;
	}
	
	

}

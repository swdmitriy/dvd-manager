package ru.megateam.dvdmanager.exception;

import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.http.HttpStatus;

@ResponseStatus(value=HttpStatus.CONFLICT)
public class DiscTakingException extends Exception {

	private static final long serialVersionUID = 6602504885049062021L;
	
    public DiscTakingException() {}

    public DiscTakingException(String message)
    {
       super(message);
    }
}

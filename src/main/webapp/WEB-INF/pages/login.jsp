<%@ page contentType="text/html; charset=UTF-8" language="java" isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<link href="<c:url value="/resources/styles.css" />" rel="stylesheet">
<title>DVD Manager</title>
<html>
<body>
<div class="login-div">
    <h1>DVD Manager</h1>
    <c:url value="/j_spring_security_check" var="loginUrl" />
    <form action="${loginUrl}" method="post">
        <h2>Вход в приложение</h2>
        <input type="text" name="j_username" placeholder="Email address" required autofocus ><br>
        <input type="password"  name="j_password" placeholder="Password" required ><br>
        <button type="submit">Войти</button>
    </form>
</div>
</body>
</html>
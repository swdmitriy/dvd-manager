<%@ page language="java" contentType="text/html; charset=utf8"
	pageEncoding="utf8" isELIgnored="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<link href="<c:url value="/resources/styles.css" />" rel="stylesheet">
<title>DVD Manager</title>
<html>
<body>
	<div class="center-div">
		<sec:authorize access="isAuthenticated()">
		<p>Ваш логин: <sec:authentication property="principal.username" />&nbsp;<a href="<c:url value="/logout" />" role="button">Выйти</a></p>
		</sec:authorize>
		<a href="${contextPath}/">Свободые диски</a>&nbsp;|&nbsp;
		<a href="${contextPath}/newdisc">Добавить новый диск</a>&nbsp;|&nbsp;
		<a href="${contextPath}/takenbyme">Диски, взятые мной</a>&nbsp;|&nbsp;
		<a href="${contextPath}/hold">Диски, взятые у меня</a>
		<c:if test="${!empty discList}">
			<table  class="discTable">
				<caption>Список дисков, взятых мной</caption>
				<tr>
					<th>Название диска</th>	
				</tr>
				<c:forEach items="${discList}" var="disc">
					<tr>
						<td>${disc[0].name}</td>
						<td>
						<form:form method="post" action="freedisc" commandName="takenItem">
							<form:input path="id" type = "hidden" value="${disc[1].id}" />
							<input type="submit" value="Отдать диск" />
						</form:form>				
						</td>
					</tr>
				</c:forEach>
			</table>
		</c:if>
		<c:if test="${empty discList}">
		<h2>У вас нет чужих дисков. Вы никому ничего не должны.</h2>
		</c:if>
	</div>
</body>
</html>
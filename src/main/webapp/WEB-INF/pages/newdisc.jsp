<%@ page language="java" contentType="text/html; charset=utf8"
	pageEncoding="utf8" isELIgnored="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<link href="<c:url value="/resources/styles.css" />" rel="stylesheet">
<title>DVD Manager</title>
<html>
<body>
	<div class="center-div">
		<sec:authorize access="isAuthenticated()">
		<p>Ваш логин: <sec:authentication property="principal.username" />&nbsp;<a href="<c:url value="/logout" />" role="button">Выйти</a></p>
		</sec:authorize>
		<a href="${contextPath}/">Свободые диски</a>&nbsp;|&nbsp;
		<a href="${contextPath}/newdisc">Добавить новый диск</a>&nbsp;|&nbsp;
		<a href="${contextPath}/takenbyme">Диски, взятые мной</a>&nbsp;|&nbsp;
		<a href="${contextPath}/hold">Диски, взятые у меня</a>
		<form:form method="post" action="newdisc" commandName="disc">
		<p>Добавление нового диска</p>
			<table>
				<tr>
					<td>Название диска</td>
					<td>
						<form:input path="name"  />
						<form:errors path="name" />
					</td>         
				</tr>		
				<tr>
					<td colspan="2">
						<input type="submit" value="Добавить" />
					</td>
				</tr>
			</table>
		</form:form>
	</div>
</body>
</html>
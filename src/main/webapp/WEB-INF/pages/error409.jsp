<%@ page language="java" contentType="text/html; charset=utf8"
	pageEncoding="utf8" isELIgnored="false" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<link href="<c:url value="/resources/styles.css" />" rel="stylesheet">
<title>DVD Manager</title>
<html>
<body>
	<div class="center-div">
		<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
			<sec:authorize access="isAuthenticated()">
			<p>Ваш логин: <sec:authentication property="principal.username" />&nbsp;<a href="<c:url value="/logout" />" role="button">Выйти</a></p>
		</sec:authorize>
		<a href="${contextPath}/">Свободые диски</a>&nbsp;|&nbsp;
		<a href="${contextPath}/newdisc">Добавить новый диск</a>&nbsp;|&nbsp;
		<a href="${contextPath}/takenbyme">Диски, взятые мной</a>&nbsp;|&nbsp;
		<a href="${contextPath}/hold">Диски, взятые у меня</a>
		<p>Похоже этот диск уже забрали! Попробуйте выбрать другой.<br>
		<a href="${contextPath}/">Вернуться к списку дисков.</a></p>
	</div>
</body>
</html>
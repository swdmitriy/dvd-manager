<%@ page contentType="text/html; charset=UTF-8" language="java" isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@page trimDirectiveWhitespaces="true"%>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<link href="<c:url value="/resources/styles.css" />" rel="stylesheet">
<title>DVD Manager</title>
<html>
<body>
	<div class="center-div">
		<sec:authorize access="isAuthenticated()">
		<p>Ваш логин: <sec:authentication property="principal.username" />&nbsp;<a href="<c:url value="/logout" />" role="button">Выйти</a></p>
		</sec:authorize>
		<a href="${contextPath}/">Свободые диски</a>&nbsp;|&nbsp;
		<a href="${contextPath}/newdisc">Добавить новый диск</a>&nbsp;|&nbsp;
		<a href="${contextPath}/takenbyme">Диски, взятые мной</a>&nbsp;|&nbsp;
		<a href="${contextPath}/hold">Диски, взятые у меня</a>
		<c:if test="${!empty discList}">
			<table class="discTable">		
				<caption>Список свободных дисков</caption>
				<tr>
					<th>Название диска</th>
					<th></th>
				</tr>
				<c:forEach items="${discList}" var="disc_item">
					<tr>
						<td>${disc_item[0].name}</td>
						<td>
						<p><c:choose>
		    				<c:when test="${disc_item[1]=='not my'}">
								<form:form method="post" action="takedisc" commandName="disc">
									<form:input path="id" type="hidden" value="${disc_item[0].id}" />
									<input type="submit" value="Взять диск" />
								</form:form>
							</c:when>    
		    				<c:otherwise>
		    					Это мой диск
		   					</c:otherwise>
						</c:choose>
						</p>
						</td>
					</tr>
					
				</c:forEach>
			</table>
		</c:if>
		<c:if test="${empty discList}">
		<h2>К сожалению, свободных дисков нет!</h2>
		</c:if>
	</div>
</body>
</html>
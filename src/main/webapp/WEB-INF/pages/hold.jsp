<%@ page language="java" contentType="text/html; charset=utf8"
	pageEncoding="utf8" isELIgnored="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<link href="<c:url value="/resources/styles.css" />" rel="stylesheet">
<title>DVD Manager</title>
<html>
<body>	
	<div class="center-div">
		<sec:authorize access="isAuthenticated()">
			<p>Ваш логин: <sec:authentication property="principal.username" />&nbsp;<a href="<c:url value="/logout" />" role="button">Выйти</a></p>
		</sec:authorize>
		<a href="${contextPath}/">Свободые диски</a>&nbsp;|&nbsp;
		<a href="${contextPath}/newdisc">Добавить новый диск</a>&nbsp;|&nbsp;
		<a href="${contextPath}/takenbyme">Диски, взятые мной</a>&nbsp;|&nbsp;
		<a href="${contextPath}/hold">Диски, взятые у меня</a>
		<c:if test="${!empty discList}">
			<table class="discTable">
				<caption>Список дисков, взятых у меня</caption>
				<tr>
					<th>Название диска</th>			
					<th>Кто взял</th>
				</tr>
				<c:forEach items="${discList}" var="disc">
					<tr>
						<td>${disc[0]}</td>
						<td>${disc[1].name}</td>
					</tr>
				</c:forEach>
			</table>
		</c:if>
		<c:if test="${empty discList}">
		<h2>Список пуст. Пока никто не заинтересовался вашими дисками.</h2>
		</c:if>
	</div>
</body>
</html>